var variables, constraints;
var count = 0;
$(function() {

	$( document ).ready(function() {
		variables = 1;
		constraints = 1;
	});


	$('#addCons').on('click',function(e){
			
		constraints++;
		createNewConstraintsTable(1);
	});

	$('#removeCons').on('click',function(e){
		if (constraints >=1){
			constraints--;
		 	$('#constraintsTable tr:last').remove();
		}
	});

	$('#addVariable').on('click',function(e){
			
		variables++;
		$("#functionTable tr:first").append('<th> c'+variables+' </th>');
		$("#functionTable tr:last").append('<td><input id="c'+variables+'" type="number" class="form-control"></td>');

		$("#constraintsTable").empty();
		
		createNewHeaderConstraintsTable();					
		createNewConstraintsTable(constraints);

	});

	$('#removeVariable').on('click',function(e){
		if (variables > 1){ 
			variables--;
		 	$("#functionTable tr:first th:last").remove();
		 	$("#functionTable td:last").remove();

		 	$("#constraintsTable th:eq("+variables+")").remove();
		 	for (var c = 1; c <= constraints; c++){
		 		$("#constraintsTable tr:eq("+c+") td:eq("+variables+")").remove();
		 	}
		}
	});

	createNewHeaderConstraintsTable = function(){

		for (v = 1; v <= variables; v++){
			if (v == 1)
				$("#constraintsTable").append("<tr>");
			
			$("#constraintsTable tr:first").append('<th> x'+v+' </th>');
		}
		$("#constraintsTable tr:first").append('<th> sinal </th>'+
					'<th class="col-xs-2"> b </th>'+
				'</tr>');
	}

	createNewConstraintsTable = function(constraintsCreated){
		
		var i = 1;
		if (constraintsCreated == 1)
			i = constraints; 

		for (var c = i; c <= constraints; c++){	

			for (var v = 1; v <= variables; v++){

				if (v == 1)
					$("#constraintsTable").append("<tr>");

				$("#constraintsTable tr:eq("+c+")").append('<td><input id="x'+c+""+v+'" type="number" class="form-control"></td>');
			}

			$("#constraintsTable tr:eq("+c+")").append('<td><select id="signal'+c+'" class="form-control">'+
								'<option id="ge">&ge;</option>'+
								'<option id="le">&le;</option>'+
								'<option id="eq">=</option>'+
							'</select>'+
						'</td>'+
						'<td class="col-xs-2"><input id="b'+c+'" type="number" class="form-control"></td>'+
					'</tr>');
		}
	}

	createTable = function(idDiv){

		$idDiv =  $("#"+idDiv);

		var n1 = n[1];
		var idTable	= "simplex";
		if (idDiv == "solutions"){
			$("#h3Solution").show();
		}else{
			n1 = 0;
			idTable = "optimalbase";
		}

		$idDiv.append('<table class="table" id="'+idTable+count+'"> <tr><th></th>');

		for (var v = 1; v <= variables+n[0]+n1; v++){
			$("#"+idTable+count+" tr:first").append("<th>x"+v+"</th>");
		}

		$("#"+idTable+count+" tr:first").append("<th>RHS</th></tr>");

		$("#"+idTable+count).append("<tr><th>z</th>");
		if (idDiv == "solutions"){
			for (var j = 0; j < costs.length; j++)
				$("#"+idTable+count+" tr:last").append("<td>"+costs[j].toFixed(2)+"</td>");
		}else{
			for (var j = 0; j < costs.length-n[1]-1; j++)
				$("#"+idTable+count+" tr:last").append("<td>"+costs[j].toFixed(2)+"</td>");
			$("#"+idTable+count+" tr:last").append("<td>"+costs[costs.length-1].toFixed(2)+"</td>");
		}

		for (var k = 0; k < occurrences.length; k++){

			$("#"+idTable+count).append("<tr><th>x"+(order[k]+1)+"</th>");
			for (var j = 0; j < A[0].length; j++){
				$("#"+idTable+count+" tr:last").append("<td>"+A[k][j].toFixed(2)+"</td>");
			}

			$("#"+idTable+count+" tr:last").append("<td>"+b[k].toFixed(2)+"</td>");
		}	
		count++;
	}

	createAlert = function(id, msg, type){
		var element = '<div class="alert alert-'+type+'" role="alert">';
		element += msg;
		element += '</div>';
		var $id = $('#'+id);
		$id.empty();
		$id.append(element);
	};
});