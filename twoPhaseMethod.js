var costs = [];
var order = [];
var multipleSol = false;
var solutionsFound = [];

$(function() {

	phaseOne = function(){
		costs = calculateCosts1();
		getBasicVariablesOrder();
		count = 0;
		simplex();
		for(var i = A[0].length; i > A[0].length - n[1]; i--){
			if ($.inArray(i-1,order) >= 0){
				createAlert("alertMessages"," PPL inviável","danger");
				return;
			}			
		}
		if (step){
			$('#solutions').append('<h4>Solução da 1ª fase</h4><br>');
			createTable("solutions");
			$('#solutions').append('<hr>');
		}
		phaseTwo();
	}

	phaseTwo = function(){

		var len = A[0].length;
		for (var k = 0; k < constraints; k++){
			A[k] = A[k].slice(0, len-n[1]);
		}
		
		$("#optimalBasePanel").show();
		createTable("optimalbase");
		n[1] = 0;
		costs = calculateCosts2();
		simplex();
		$('#solutions').append('<h4>Solução(ões) da 2ª fase</h4><br>');
		createTable("solutions");
		verifySolution();
	}

	simplex = function(){

		var getOut, getIn;
		
		while (isPositive() || multipleSol){
			
			if (step)	
				createTable("solutions");
			
			if (multipleSol)
				getIn = findGetInCostZero();
			else
				getIn = findGetIn();
			
			if (getIn < 0)
				break;

			mostPositive = costs[getIn];
			
			if (mostPositive < 0)
				break;

			lessPositive = b[0]/ A[0][getIn]; 
			getOut = 0;
			for (var k = 0; k <constraints; k++){
							
				if(lessPositive < 0 || (A[k][getIn] >0 && b[k]/A[k][getIn] >= 0 && (b[k]/A[k][getIn]).toFixed(1) <lessPositive)){
					lessPositive = b[k]/A[k][getIn];
					getOut = k;
				}
			}
			if(lessPositive < 0 || A[getOut][getIn] == 0)
				break;

			order[getOut] = getIn;
			
			var pivot = A[getOut][getIn];
			var pivotLine =	 [];
			for (var k = 0; k < A[0].length; k++)
				pivotLine[k] = A[getOut][k];

			var factor = costs[getIn];
			for(var l = 0; l < costs.length-1; l++){
				costs[l] += - (factor/pivot)*pivotLine[l];
			}
			costs[costs.length-1] -= (factor/pivot)*b[getOut];
			
			var n = b[getOut];
			for (var k = 0; k < constraints; k++){
				var factor = A[k][getIn];
				
				for(var l = 0; l < A[0].length; l++){
					if (k == getOut)
						A[k][l] /= pivot;
					else
						A[k][l] +=  -(factor/pivot)*pivotLine[l];	
				}

				if (k == getOut)
					b[k] /= pivot;
				else
					b[k] += -(factor/pivot)*n;
			}
			multipleSol = false;
		}
	}


	findGetIn = function(){

		var mostPositive = costs[0];
		var getIn = 0;
		for (var i= 1; i< costs.length-1; i++){
			if (costs[i] > mostPositive){
				mostPositive = costs[i];
				getIn = i;
			}
		}
		return getIn;
	}

	findGetInCostZero = function(){
		var getIn = -1;
		for (var i= 1; i< A[0].length; i++){
			if ($.inArray(i,order) == -1){
				if (costs[i] == 0){
					getIn = i;
					break;
				}
			}
		}
		return getIn;
	}

	calculateCosts1 = function(){
		var costs = [];
		var aux = [];
		var cb = [];

		for (var k = 0; k < occurrences.length; k++){		
			cb[k] = 0;
			if (occurrences[k] == -1)
				cb[k] = 1;
		}

		for (var i = 0; i < A[0].length; i++ ){

			for (var k = 0; k < constraints; k++){
				aux[k] = A[k][i];	
			}
			
			if (i+n[1] < A[0].length)
				costs[i] = math.multiply(cb, aux); 
			else
				costs[i] =  math.multiply(cb, aux) - 1;
		}
		
		costs[A[0].length] = math.multiply(cb, b);

		return costs;
	}

	calculateCosts2 = function(){

		var cb = [], aux =[];

		for (var i = 0; i < order.length; i++){		
			if ( c[order[i]] === undefined)
				cb[i] = 0;
			else
				cb[i] = c[order[i]];
		}
		
		var costs = new Array();
		for (var i = 0; i < A[0].length; i++){

			for (var k = 0; k < constraints; k++){
				aux[k] = A[k][i];
			}
			costs[i] = -(c[i] - math.multiply(cb, aux));
			if (c[i] === undefined)
				costs[i] = math.multiply(cb, aux);
		}

		costs[costs.length] = math.multiply(cb,b);
		
		return costs;
	}

	isPositive = function(){

		for (var i= 0; i<costs.length-1; i++){
			if (costs[i] > 0)	
				return true;
		}
		return false;
	}

	getBasicVariablesOrder = function(){

		var aux = [];	index = 0;
		for (var k = 0; k < constraints; k++){	
			for (var i = A[0].length-1; i > A[0].length-n[0]-n[1]; i--){			
				if (A[k][i] ==	1){
					aux[index++] = i;
				}
			}
		}
		order = aux;
	}

	verifySolution = function(){

		var indexZero =[]; var j = 0;
		var lessThanZero = 0;
		for (var i = 0; i < costs.length-1; i++){
			if ( $.inArray(i,order) == -1 ){
				if (costs[i] < 0)
					lessThanZero++;
				else
					indexZero[j++] = i;
			}
		}

		if (lessThanZero == A[0].length - order.length){
			createAlert("typeSolution"," Solução Única","info");
		}else{
			var count = 0;
			var yrLessThanZero = 0;
			for (var i = 0; i < indexZero.length; i++){
				yrLessThanZero = 0;
				for(var k =0; k < constraints; k++){
					if ( A[k][indexZero[i]] < 0){
						yrLessThanZero++;
					}
				}
				if (yrLessThanZero == constraints)
					count++;
			}

			if (isPositive()){
					createAlert("typeSolution","Solução Ilimitada","info");
			}else{
				if (count == indexZero.length){
					createAlert("typeSolution"," Infinitas Soluções","info");
				}			
				else{
					var l = 0;
					if (!test()){
						solutionsFound[l++] = [b,costs[costs.length-1]];
						multipleSol = true;
						simplex();
						createTable("solutions");
					}
					createAlert("typeSolution"," Múltiplas Soluções","info");
					multipleSol = false;
				}
			}
		}
	}
	
	test = function(){
		for (var i = 0; i <solutionsFound.length; i++){
			if (math.deepEqual(solutionsFound[i],[b,costs[costs.length-1]]))
				return true;
		}
		return false;
	}
});