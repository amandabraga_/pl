var b = [];
var c = [];
var A = new Array();
var n = []; //[numero de variaveis de folga, numero de variaveis artificiais]
var occurrences = [];
var step;

$(function() {

	$('#solveStep').on('click',function(e){
		step = true;
		prepareSolving();
	});

	$('#solve').on('click',function(e){
		step = false;
		prepareSolving();
	});

	prepareSolving = function(){

		$("#solutions").empty();
		$("#optimalbase").empty();
	 	$("#alertMessages").empty();
		
		b = new Array();
		for(var k = 1; k <= constraints; k++){

	 		b[k-1] = getValue($("#b"+k).val());
	 		if (b[k-1] < 0){
	 			createAlert("alertMessages"," b deve >= 0","danger");
	 			return;
	 		}
	 	}

	 	var factor = 1;
	 	if ($("#minmax option:selected").attr('id') == "max")
	 		factor = -1;
		c = new Array();
	 	for(var v = 1; v <= variables; v++){	
	 		c[v-1] = factor*getValue($("#c"+v).val());
	 	}

	 	createA();
	}

	createA = function(){
		A = new Array();
		var count = 0;
		occurrences = new Array();
		for(var k = 1; k <= constraints; k++)
				occurrences[k-1] =-1;

		for(var k = 1; k <= constraints; k++){
 			
 			A[k-1] = new Array();

 			for(var v = 1; v <= variables + constraints; v++){
 				if (v <= variables){
 					A[k-1][v-1] =  getValue($("#x"+k+""+v).val());
 				}
 				else{
 					A[k-1][v-1] = 0;

 					if ( (v - variables) == k){
 						id = $("#signal"+k+" option:selected").attr('id');
 						if ( id == "le")
 							A[k-1][v-1] = 1;
 						if ( id == "ge")
 							A[k-1][v-1] = -1;
 					}

 					if (A[k-1][v-1] == 1){
 						occurrences[k-1] = 1;
 						count++;
 					}
 				}
	 		}
	 	}
	 	n = [A[0].length - variables, constraints - count];
	 	if (count != constraints){
	 		createArtificialVariables(occurrences, count);
	 	}
	 	else{
	 		for (var i = 0; i < A[0].length - variables; i++)
	 			order[i] = A[0].length - i -1;	
	 		order.reverse();
	 		phaseTwo();
		}
	}

	getValue = function(str){

	 	if(!str.trim() || isNaN(parseInt(str)))
	 			str = 0;
	 	return parseInt(str);

	}

	createArtificialVariables = function(occurrences, count){

		var sum = A[0].length+(constraints-count-1);
		var m =	A[0].length;
		var l = m;
		var b;

		for(var k = 1; k <= constraints; k++){
			b = false;
			for (var j = m; j <= sum; j++){
				A[k-1][j] = 0;
				for (var i = constraints - 1; i >= 0; i--) {
					if (occurrences[i] == -1 && (l == j) && (i == k-1) && !b){
						A[k-1][j] = 1;
						b = true;
						l++;
					}
				}					
			}
		}
		phaseOne();
	}

});
